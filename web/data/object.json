[
    {
        "description":"<b>WARNING</b>",
        "blocks":[
            {
                "blockquote":"Object Detection uses a lot of CPU Power. To mitigate this you can use an NVIDIA Graphics Processor. A desktop GPU, server-class GPU, or even Jetson Nano's tiny little Maxwell. Be sure to install the drivers before installing the plugin. Jetson users do not need to install CUDA drivers, they are already installed."
            }
        ]
    },
    {
        "description":"Install the NVIDIA Drivers and CUDA Toolkit <small>(Ubuntu - Easy)</small>",
        "blocks":[
            {
                "blockquote":"<b>WARNING : </b> Only do this if you have an NVIDIA graphics chip."
            },
            {
                "blockquote":"<a href='https://hub.shinobi.video/articles/view/jPEYnfOFEYdcss7'>More Versions of CUDA that you can Install</a>"
            },
            {
                "orderList":[
                    {
                        "text":"Install NVIDIA Drivers and CUDA Toolkit",
                        "code":"curl -s https://gitlab.com/Shinobi-Systems/Shinobi/raw/dev/INSTALL/cuda-10.sh | sudo sh"
                    },
                    {
                        "text":"Reboot.",
                        "code":"sudo reboot"
                    }
                ]
            },
            {
                "blockquote":"<a href='https://hub.shinobi.video/articles/view/jAT07atp3DPl9Qq'>Could not load dynamic library 'libcudart.so.10.0'; dlerror: libcudart.so.10.0: cannot open shared object file: No such file or directory</a>"
            },
            {
                "orderList":[
                    {
                        "text":"Install NVIDIA Drivers and CUDA Toolkit",
                        "code":"curl -s https://gitlab.com/Shinobi-Systems/Shinobi/raw/dev/INSTALL/cuda-10.sh | sudo sh"
                    },
                    {
                        "text":"Reboot.",
                        "code":"sudo reboot"
                    }
                ]
            }
        ]
    },
    {
        "description":"Install the <b>TensorFlow</b> plugin <b>with</b> or <b>without</b> CUDA <small>(Ubuntu 19.10 - Easy)</small>",
        "blocks":[
            {
                "text":"<iframe width='560' height='315' src='https://www.youtube.com/embed/pQjTp8TgHn8' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>"
            },
            {
                "blockquote":"TensorFlow is the most refined detection method available with Shinobi. There are numerous model sets you can choose from. The default ones provided with the installer are general purpose and detect a number of different things."
            },
            {
                "list":[
                    {
                        "text":"<a href='https://gitlab.com/Shinobi-Systems/Shinobi/tree/dev/plugins/tensorflow'>TensorFlow Plugin Installation Guide</a>"
                    }
                ]
            },
            {
                "text":"<iframe width='560' height='315' src='https://www.youtube.com/embed/8MRAS1kZ9b8' frameborder='0' allow='accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>"
            },
            {
                "text":"<iframe width='560' height='315' src='https://www.youtube.com/embed/EVPc2B2MUkc' frameborder='0' allow='accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>"
            }
        ]
    },
    {
        "description":"Install OpenCV <b>without</b> CUDA <small>(Ubuntu - Easy)</small>",
        "blocks":[
            {
                "blockquote":"OpenCV is an engine used for detection, you should opt to use TensorFlow instead though. This is the same install script used in the CUDA installation but if you do not have the NVIDIA Drivers and CUDA Toolkit it will install OpenCV without it."
            },
            {
                "orderList":[
                    {
                        "text":"Install libraries",
                        "code":"curl -s https://gitlab.com/Shinobi-Systems/Shinobi/raw/dev/INSTALL/opencv-cuda.sh | sudo sh"
                    }
                ]
            }
        ]
    },
    {
        "description":"Install OpenCV <b>with</b> CUDA <small>(Ubuntu - Easy)</small>",
        "blocks":[
            {
                "blockquote":"<b>WARNING : </b> Only do this if you have an NVIDIA Graphics Processor and have ran the install script provided <a href='https://shinobi.video/docs/object#content-install-the-nvidia-drivers-and-cuda-toolkit-smallubuntu--easysmall'>above</a> for the Drivers and Toolkit. If you already have them installed please be sure they are compatible with the plugins. You should opt to use TensorFlow instead."
            },
            {
                "orderList":[
                    {
                        "text":"Check the driver status with NVIDIA's utility.",
                        "code":"nvidia-smi"
                    },
                    {
                        "text":"nvidia-smi should show you something like this.",
                        "code":"+-----------------------------------------------------------------------------+<br>| NVIDIA-SMI 390.25                 Driver Version: 390.25                    |<br>|-------------------------------+----------------------+----------------------+<br>| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |<br>| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |<br>|===============================+======================+======================|<br>|   0  GeForce GTX 1050    Off  | 00000000:01:00.0 Off |                  N/A |<br>| 30%   30C    P0    N/A /  75W |      0MiB /  1999MiB |      0%      Default |<br>+-------------------------------+----------------------+----------------------+<br>                                                                               <br>+-----------------------------------------------------------------------------+<br>| Processes:                                                       GPU Memory |<br>|  GPU       PID   Type   Process name                             Usage      |<br>|=============================================================================|<br>|  No running processes found                                                 |<br>+-----------------------------------------------------------------------------+"
                    },
                    {
                        "text":"Download, Build and Install OpenCV with CUDA enabled.",
                        "code":"curl -s https://gitlab.com/Shinobi-Systems/Shinobi/raw/dev/INSTALL/opencv-cuda.sh | sudo sh"
                    },
                    {
                        "text":"<b>Driver not starting?</b> You may need to reboot. If not then you may need to install version 340 instead.",
                        "code":"sudo apt install nvidia-340* -y"
                    }
                ]
            }
        ]
    },
    {
        "description":"Install <b>OpenALPR</b> with CUDA <small>(Ubuntu - Easy)</small>",
        "blocks":[
            {
                "blockquote":"<b>WARNING : </b> Be sure you have OpenCV built and installed with CUDA."
            },
            {
                "orderList":[
                    {
                        "text":"Download, Build and Install OpenALPR with CUDA enabled.",
                        "code":"curl -s https://gitlab.com/Shinobi-Systems/Shinobi/raw/dev/INSTALL/openalpr-gpu-easy.sh | sudo sh"
                    }
                ]
            }
        ]
    },
    {
        "description":"Install a Shinobi Plugin",
        "blocks":[
            {
                "blockquote":"We will use TensorFlow as the example for this section. You may need <code>sudo</code> infront of some commands if your user doesn't have permission to execute them."
            },
            {
                "orderList":[
                    {
                        "text":"Navigate to your plugin's directory, where you downloaded the Shinobi files to.",
                        "code":"<i>Example : </i>cd /home/Shinobi/plugins/tensorflow"
                    },
                    {
                        "text":"Navigate to your Shinobi directory and install the node.js wrappers needed to run the plugin. Some plugins will complete the conf.json modifications for you within this installer.",
                        "code":"sh INSTALL.sh"
                    },
                    {
                        "text":"Modify the conf.json <code>key</code> value to anything you want, make it random.",
                        "code":"nano conf.json"
                    },
                    {
                        "text":"Open your main conf.json.",
                        "code":"nano /home/Shinobi/conf.json"
                    },
                    {
                        "text":"Add the key you created to allow the plugin to authenticate. <code>TensorFlow123123</code> is the default key provided in the <code>conf.json</code> for the TensorFlow plugin.",
                        "code":"\"pluginKeys\":{\"TensorFlow\":\"TensorFlow123123\"}"
                    },
                    {
                        "text":"Shinobi will need to be restarted after modifying connection keys",
                        "code":"pm2 restart camera"
                    },
                    {
                        "text":"Start the plugin",
                        "code":"pm2 start shinobi-tensorflow.js"
                    },
                    {
                        "text":"When complete you will see <code>Object Detection : TensorFlow Connected</code> in your Monitor Settings."
                    }
                ]
            }
        ]
    },
    {
        "description":"Once Installed",
        "blocks":[
            {
                "text":"Important information to note are as follows."
            },
            {
                "list":[
                    {
                        "text":"<code>Monitor Settings > Global Detector Settings > Send Frames : </code> Enabling this will push frames to your main detection."
                    },
                    {
                        "text":"<code>Monitor Settings > Global Detector Settings > Motion Detection : </code> Disabling this will make your Object Detection as the primary detection engine."
                    },
                    {
                        "text":"<code>Monitor Settings > Global Detector Settings > Object Detection > Send Frames : </code> Enabling this will push frames to your object detection engine."
                    },
                    {
                        "text":"<code>Monitor Settings > Global Detector Settings > Object Detection > Check For Motion First : </code> Enabling this will force check for motion before sending any images to the object detector."
                    }
                ]
            }
        ]
    },
    {
        "description":"Why aren't plugins just integrated?",
        "blocks":[
            {
                "text":"Not everyone uses object detection and the libraries required can be bothersome to install, based on the OS. For example if I make shinobi-opencv.js a feature then everyone will be required to install OpenCV just to use basic features in Shinobi. This also allows us to swap the plugin with a custom one or run it on another machine entirely. Sharing the work between multiple machines can be a great way to optimize performance."
            }
        ]
    }
]
